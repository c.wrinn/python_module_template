#!/usr/bin/python

############################################################
#
# Copyright {year} {author}
#           All Rights Reserved
#
############################################################

from .main import run

# Imports
from os import environ
import logging
import os

# Functions
def parse_args():
    from argparse import ArgumentParser
    parser = ArgumentParser(description="{description}")
    verbosity_group.add_argument(
        '-d', '--debug',
        action='store_const', dest='loglevel', const=logging.DEBUG,
        default=logging.WARNING,
    )
    verbosity_group.add_argument(
        '-v', '--verbose',
        action='store_const', dest='loglevel', const=logging.INFO,
    )
  #parser.add_argument('--example', '-e')
  return parser.parse_args()


# Main
# Where we put the logic if this script is run on its own.
def main():
    # By default, set our logging level to debug
    logging.basicConfig(level=logging.DEBUG)

    # Parse any args
    args = parse_args()
    assert args

    # And raise an obvious error that the script hasn't been built yet.
    run(args)

# If this was called from the command line, run main. Otherwise it is a module.
if __name__ == '__main__':
  main()
