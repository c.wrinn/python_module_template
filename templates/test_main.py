#!/usr/bin/python

# Imports
from unittest import TestCase
import code.main
import sys

# Tests
class Test_Main(TestCase):
    def test_run(self):
        code.main.run()
        self.assertEquals('Hello, Beautiful World!', sys.stdout.getvalue().strip())


