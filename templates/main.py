#!/usr/bin/python

############################################################
#
# Copyright {year} {author}
#           All Rights Reserved
#
############################################################

if __name__ == '__main__':
  raise NotImplementedError('This is not a console script.')

# Imports
import logging

main_logger = logging.getLogger('root').getChild('{name}.main')

# Functions
def run(params={}):
    main_logger.debug('Entering main.run')
    print('Hello, Beautiful World!')

