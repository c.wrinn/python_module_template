#!/usr/bin/python
if __name__ != '__main__':
    raise Exception("This is meant to be run as a script.")

from string import Template
import logging
import os
import argparse
import subprocess
import datetime

parser = argparse.ArgumentParser(description="Used to initialize a project.")

parser.add_argument('name', required=True)
parser.add_argument('location', default='.')

parser.add_argument('--debug',       '-d', action='store_true')
parser.add_argument('--verbose',     '-v', action='store_true')
parser.add_argument('--description', '-D')
parser.add_argument('--author',      '-A',
                    default=subprocess.check_output(['git', 'config', 'user.name']).strip())
parser.add_argument('--email',       '-E',
                    default=subprocess.check_output(['git', 'config', 'user.email']).strip())
parser.add_argument('--keywords',    '-K')
parser.add_argument('--url', '-u', default='www.example.com')
parser.add_argument('--year', '-Y', default=datetime.datetime.now().year)


opts = vars(parser.parse_args())
log_level = logging.ERROR

if 'verbose' in opts:
    log_level = logging.INFO

if 'debug' in opts:
    log_level = logging.DEBUG

logging.basicConfig(level=log_level)
logging.debug('Parsed Opts: %s', opts)

logging.info('Checking if output folder is writeable....')
if not os.path.isdir(opts.get('location'))
   and not os.access(opts.get('location'), os.W_OK):
       logging.error('Output folder is not writeable.')

files = (
         'Pipfile',
         'setup.py',
         'code/__init__.py',
         'code/command_line.py',
         'code/main.py',
         'tests/__init__.py',
         'tests/test_main.py',
        )
for f in files:
    template_file = 'templates/{base}'.format(base=os.path.basename(f))

    logging.info('Reading template %s', template_file)
    with open(template_file, 'r') as t:
        template = Template(t.read())

    d = os.path.dirname(f)
    if d and not os.path.isdir(d):
        logging.info('Creating %s', d)
        os.makedirs(d, exist_ok=True)

    logging.info('Writing %s', f)
    with open(f, 'w') as w:
        print(template.safe_substitute(**opts), file=w)

    os.remove(template_file)

print('Initialization finished without error.',
      'Check the results before committing.',
      'If unsatisfied, reset using `git reset --hard`.',
      'If everything looks fine, do:',
      '    git rm --cached initialize.py',
      '    git add .',
      '    git commit -m "Initial commit."',
      'Enjoy! :)',
      sep="\n")
